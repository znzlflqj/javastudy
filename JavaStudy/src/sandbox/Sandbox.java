package sandbox;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Sandbox {
	
	public static void main(String[] args) {
		System.out.println(localDateAndTime());
		openURL("https://finance.naver.com/sise/sise_index.nhn?code=KOSPI");
	}
	
	// current local date and time
	private static String localDateAndTime() {
		DateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss:ms");
		Date d = new Date();
		return timeFormat.format(d);
	}
	
	// open a site
	private static void openURL(String inputURL) {
		try {
			URL url = new URL(inputURL);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			
			con.setRequestMethod("GET");
			
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
			String line = null;
			while((line = br.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			System.out.println("Failed to open a URL");
			e.printStackTrace();
		}
	}
	
}

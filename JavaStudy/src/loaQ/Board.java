package loaQ;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Board extends JFrame{
	
	
	final int panelCount = 3;	// 프레임 안 패널 개수

	
	
	private JFrame jFrame;
	private JPanel jPanel[] = new JPanel[panelCount];
	private JTextArea txtlog;
	private JScrollPane scrollPane;
	private JButton btn1 = new JButton("버튼1");
	
	public Board () {
		makeFrame();
		makePanel();
		fillPanel(0,"test1");
		
		jFrame.add(jPanel[0]);
		//jFrame.pack();
		jFrame.setVisible(true);
	}
	
	private void makeFrame() {
		jFrame = new JFrame("Testing GUI");
		jFrame.setBounds(100, 100, 600, 500);
		jFrame.setLocation(200,200);
		jFrame.setLayout(null);
	}
	
	private void makePanel() {
		for (int i=0; i<panelCount; i++) {
			jPanel[i] = new JPanel();
		}
	}
	
	private void fillPanel(int panelNumber, String title) {
		jPanel[panelNumber].setBorder(BorderFactory.createEmptyBorder(0,10,10,10));
		JLabel jLabel = new JLabel(title);
		jLabel.setPreferredSize(new Dimension(100,30));
		jPanel[panelNumber].setBounds(30, 30, 200, 50);
		jPanel[panelNumber].add(jLabel);
		jPanel[panelNumber].add(new JTextField());
	}
	
	
	
	
	
}

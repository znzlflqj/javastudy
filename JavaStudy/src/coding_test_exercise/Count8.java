package coding_test_exercise;

import java.util.Scanner;

public class Count8 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int number = 0;
		boolean isValidNumber = false;
		
		while (!isValidNumber) {
			
				System.out.print("숫자를 입력하시오. => ");
				if (sc.hasNextInt()) {
					number = sc.nextInt();
					isValidNumber = true;
				} else {
					sc.nextLine();
					System.out.println("숫자만 입력하시오.(0~N)");
				}
		}
		
		int[] numArray = new int[number];
		for (int i=1; i<=number;i++) {
			numArray[i-1] = i;
		}
		
		int eightCount = 0;
		for (int i=0; i<number; i++) {
			while (numArray[i] > 0) {
				if (numArray[i] % 10 == 8) {
					eightCount++;
				}
				numArray[i] /= 10;
			}
		}
		
		System.out.println("8의 개수: " + eightCount);
		
	}
}

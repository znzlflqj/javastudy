package pythonChallenge;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

// 86 번째 문구 바뀜
// 16044/2 = 8022
// 55번째 스트링 처리 잘못할 시 에러
// 165번째 peak.html
public class PythonChallenge04 {
	public static void main(String[] args) {
		BufferedReader buff = null;
		String node = "8022";
		int i=0;
		while (true) {
			try {
				i++;
				System.out.println(i + " 번째 node : " + node);
				URL obj = new URL("http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing=" + node);
				//System.out.println("opening: http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing=" + node);
				HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

				connection.setRequestMethod("GET");

				buff = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

				String line;

				while ((line = buff.readLine()) != null) {
					System.out.println(line);
					node = line.substring(line.lastIndexOf(" ") + 1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (buff == null) {
					try {
						buff.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
}

package ch04;

public class Exam4_4_1 {
	public static void main(String[] args) {
		// 코드 작성
		// 1 ~ 99 까지의 합 구하기
		int sum = 0;
		for (int i=1; i<100; i++) {
			sum += i;
		}
		System.out.println(sum);
	}
}

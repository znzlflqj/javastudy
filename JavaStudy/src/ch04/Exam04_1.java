package ch04;

public class Exam04_1 {
	public static void main(String[] args) {
		int workingTime = 7; // 근무시간

		// 코드 작성
		// workingTime 이 8 이상이면 퇴근
		// workingTime 이 8 미만이면 계속 근무
		
		System.out.println(workingTime >= 8 ? "퇴근" : "근무");
	}
}
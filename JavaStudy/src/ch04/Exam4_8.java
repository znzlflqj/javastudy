package ch04;

public class Exam4_8 {
	public static void main(String[] args) {
		for (int i = 1; i <= 1000; i++) {
			// 코드 작성
			// 1 ~ 1000 사이의 숫자 중 3을 포함하고 있는 숫자를 출력
			if (i % 10 == 3 || i / 10  % 10 == 3 || i / 100 % 10 == 3) {
				System.out.println(i);
			}
		}
	}
}
package ch04;

import java.util.Calendar;

public class CalenderExample {
	public static void main(String[] args) {
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int date = c.get(Calendar.DATE);
		int lastDate = c.getActualMaximum(Calendar.DATE);
		int week = c.get(Calendar.DAY_OF_WEEK);
		// 월은 0 ~ 11
		System.out.println(year);
		System.out.println(month);
		System.out.println(date);
		System.out.println(lastDate);
		System.out.println(week);
		
		c.set(year, month, 1);
		week = c.get(Calendar.DAY_OF_WEEK);
		
		String weekdays = "일월화수목금토";
		for (int i=0; i<weekdays.length(); i++) {
			System.out.print(weekdays.charAt(i));
			System.out.print(i != 6 ? "\t" : "\n");
		}
		int space = week -1;
		for (int i=0; i<space; i++) {
			System.out.print("\t");
		}
		for (int i=1; i<=lastDate; i++, week++) {
			System.out.print(i);
			if (week % 7 == 0) {
				System.out.println();
			} else {
				System.out.print("\t");
			}
		}
		
	}
}

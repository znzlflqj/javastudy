package ch01;

public class Test {
	public static void main(String[] args) {
		// unix time
		System.out.println(System.currentTimeMillis());
		System.out.println((long) Math.pow(2, 38));
		
		// g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. 
		// bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. 
		// sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj.
		
		String sent = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj.";
		String sent2 = "";
		for (int i=0; i < sent.length(); i++) {
			if (sent.charAt(i) >= 'a' && sent.charAt(i) <= 'z') {
				if (sent.charAt(i)+2 > 'z') {
					sent2 += (char) (sent.charAt(i) + -24);
				} else {
					sent2 += (char) (sent.charAt(i) + 2);
				}
			} else {
				sent2 += sent.charAt(i);
			}
		}
		System.out.println(sent2);


	}
}
